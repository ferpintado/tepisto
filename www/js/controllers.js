angular.module('tepisto.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PromocionesCtrl', function($scope, $firebaseArray, FIREBASE_OBJ, $ionicSlideBoxDelegate) {
  var ref = FIREBASE_OBJ;
  var promociones = $firebaseArray(FIREBASE_OBJ.child('promociones'));
  $scope.promociones = promociones;
  $scope.loaded = false;

  promociones.$loaded(function() {
    $scope.loaded = true;
    $ionicSlideBoxDelegate.update();
  });

})

.controller('CatalogoCtrl', function($scope, Categories){

  var categoriesObj = Categories.all();

  $scope.categories = [];

  categoriesObj.$loaded(function(){
    while (categoriesObj.length) {
        $scope.categories.push(categoriesObj.splice(0, 3));
    }
  })


})

.controller('CategoryCtrl', function($scope, $stateParams, $firebaseArray, FIREBASE_OBJ){
  console.log($stateParams)
  $scope.categoryName = $stateParams.categoryName;

  var catId = $stateParams.categoryId;

  $scope.products = $firebaseArray(FIREBASE_OBJ.child('categorias/' + catId + '/products'));

  // console.log(products);



})
;

