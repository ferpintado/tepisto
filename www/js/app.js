"use strict";
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('tepisto', ['ionic', 'firebase', 'tepisto.controllers', 'tepisto.config', 'tepisto.services'])

.run(function($ionicPlatform, Categories) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });
  Categories.getAll();
  console.log("here");
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.promociones', {
    url: '/promociones',
    views: {
      'promoContent': {
        templateUrl: 'templates/promociones.html',
        controller: 'PromocionesCtrl'
      }
    }
  })

  .state('app.catalogo', {
      url: '/catalogo',
      views: {
        'catContent': {
          templateUrl: 'templates/catalogo.html',
          controller: 'CatalogoCtrl'
        }
      }
    })

  .state('app.categoria', {
    url: '/catalogo/:categoryId/:categoryName',
    views: {
      'catContent': {
        templateUrl: 'templates/categoria.html',
        controller: 'CategoryCtrl'
      }
    }
  })
  .state('app.pedidos', {
    url: '/pedidos',
    views: {
      'pedidosContent': {
        templateUrl: 'templates/pedidos.html',
        controller: 'CategoryCtrl'
      }
    }
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/promociones');
});
