angular.module('tepisto.services', [])

.factory('Categories', function(FIREBASE_OBJ, $firebaseArray) {

  var categories;

  function getCategories(){
    categories = $firebaseArray(FIREBASE_OBJ.child('categorias'));
    console.log(categories);

  }

  return {
    getAll: function(){
      getCategories();
    },
    all: function() {
      return categories;
    },
    get: function(productId) {
      // for (var i = 0; i < products.length; i++) {
      //   if (products[i].id === parseInt(productId)) {
      //     return products[i];
      //   }
      // }
      // return null;
    }
  };
});
